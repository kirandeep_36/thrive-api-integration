<!-- sidebar menu -->
<?php
    // $current_user_data = $this->session->login;
    // if ($this->session->admin) {
 ?>
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-sliders" aria-hidden="true"></i>Profile ID's<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url(); ?>dashboard">Add New</a></li>
                      <li><a href="<?php echo base_url(); ?>profiles">View All</a></li>
                    </ul>
                    <li><a><i class="fa fa-sliders" aria-hidden="true"></i>Logs<span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="<?php echo base_url(); ?>logs">View All</a></li>
                      </ul>
                    </li>

                  </li>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->
            <!-- /menu footer buttons -->

            <div class="sidebar-footer hidden-small">
              <!-- <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a> -->
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo base_url().'logout'?>">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>

            <!-- /menu footer buttons -->

          </div>

        </div>

      <?php //} ?>
        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
              <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="<?php echo base_url().'logout'?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                </li>
                <!-- <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> -->
                    <!-- <img style="height: 32px; width: 32px;" src="<?php //echo base_url().'application/uploads/'.$current_user_data['user_img_url']; ?>" alt=""><?php //echo $current_user_data['name']; ?>
 -->
                    <!-- <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li></li>
                  </ul>
                </li> -->
              </ul>

            </nav>

          </div>

        </div>

        <!-- /top navigation -->