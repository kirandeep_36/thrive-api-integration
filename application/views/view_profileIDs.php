<body>
   <div class="container body">
   <div class="main_container">
   <!-- page content -->
   <div class="right_col" role="main">
      <div class="">
         <span class="section"><?php echo $heading; ?></span>
         <div class="clearfix"></div>
         <?php 
            $msg = $this->session->flashdata('success');
                     
            if($msg){ ?>
         <div class="alert alert-success alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            <strong><?php echo $msg; ?></strong>
         </div>
         <?php 
            } elseif($this->session->flashdata('error')) { ?>
         <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            <strong><?php echo $this->session->flashdata('error'); ?></strong>
         </div>
         <?php }
            ?>
         <div class="col-md-12 col-sm-12 col-xs-12">
            <table id="dateatable" class="table table-striped table-bordered">
               <thead>
                  <tr>
                     <th>ID</th>
                     <th>Store Name</th>
                     <th>Store ID</th>
                     <th>Profile ID</th>
                     <th>Time Stamp</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                  <?php 
                     $data = $table_data;
                     
                     foreach ($data as $key => $value) {
                       echo "<tr>".
                         "<td>".$value['id']."</td>".
                         "<td>".$value['store_name']."</td>".                     
                         "<td>".$value['store_id']."</td>".                     
                         "<td>".$value['profile_id']."</td>".                     
                         "<td>".$value['time_stamp']."</td>".                     
                         "<td><a href='".base_url()."edit/".$value['id']."'><i class='fa fa-edit'></i> </a><a href='".base_url()."delete/".$value['id']."'> <i class='fa fa-remove'></a></td>".
                       "</tr>";
                     }
                     
                     ?>
               </tbody>
            </table>
         </div>
      </div>
   </div>