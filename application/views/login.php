<!DOCTYPE html>

<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> <?php echo $title; ?>  </title>
    <!-- Bootstrap -->
    <link href="<?php echo base_url(); ?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url(); ?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url(); ?>vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo base_url(); ?>vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="<?php echo base_url(); ?>vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<?php echo base_url(); ?>vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="<?php echo base_url(); ?>vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>vendors/select2/dist/css/select2.min.css" rel="stylesheet" />
    <!-- Custom Theme Style -->
    <link href="<?php echo base_url(); ?>build/css/custom.min.css" rel="stylesheet">
  </head>
  <body class="login ">
    <div class="container body">
          <div class="login_wrapper">
            <div class="animate form login_form">
              <section class="login_content">
                <?php 

                    //echo "<pre>";
                    if ($this->session->login) {
                      $current_user_data = $current_user[0];
                    }
                    //print_r($current_user_data);
                    //echo "</pre>";
                    $msg = $this->session->flashdata('success');
                    $error_msg = $this->session->flashdata('error');

                    if($msg){ ?>
                        <div class="alert alert-success alert-dismissible fade in" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>

                          <strong><?php echo $msg; ?></strong>
                        </div>
                      <?php 
                      $this->session->flashdata('');
                      }

                      if($error_msg){ ?>
                        <div class="alert alert-danger alert-dismissible fade in" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>

                          <strong><?php echo $error_msg; ?></strong>
                        </div>
                    <?php  }
                    $this->session->flashdata('');
                 ?> 
                <form method="POST" action="<?php echo base_url().'api/user_login'?>">
                  <h1><?php echo $heading; ?></h1>
                  <div>
                    <input type="text" class="form-control" name="email" placeholder="Email Address" required="" />
                  </div>
                  <div>
                    <input type="password" class="form-control" placeholder="Password" required="" name="password" />
                  </div>
                  <div>
                    <input type="submit" name="login" class="to_register btn btn-default" value="Login">
                    <!-- <a class="reset_pass" href="#">Lost your password?</a> -->
                  </div>

                  <div class="clearfix"></div>

                  <div class="separator">
                    <!-- <p class="change_link">New to site?
                      <a href="#signup" class="to_register"> Create Account </a>
                    </p> -->

                    <div class="clearfix"></div>
                    <!-- <br /> -->

                    <div>
                      <h1><i class="fa fa-paw"></i> <TABLE></TABLE>Thrivenetmarketing</h1>
                      <p>All &copy; rights <a href="<?php echo base_url(); ?>">aso.thrivenetmarketing.com</a></p>
                    </div>
                  </div> 
                </form>
              </section>
            </div>
          </div>
        </div>
      <!-- footer content -->
        <!-- <footer> -->
          <!-- <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div> -->
          <div class="clearfix"></div>
        <!-- </footer> -->
        <!-- /footer content -->
    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url(); ?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url(); ?>build/js/custom.min.js"></script>
  </body>
</html>