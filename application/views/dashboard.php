<body>
   <div class="container body">
   <div class="main_container">
   <!-- page content -->
   <div class="right_col" role="main">
      <div class="">
         <div class="clearfix"></div>
         <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
               <?php 
                  $msg = $this->session->flashdata('success');
                  if($msg){ ?>
               <div class="alert alert-success alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong><?php echo $msg; ?></strong>
               </div>
               <?php 
                  } 
                  
                  ?>
               <?php if(validation_errors()) { ?>
               <div class="alert alert-danger alert-dismissible fade in" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <strong><?php echo validation_errors(); ?></strong>
               </div>
               <?php }
                  $this->session->flashdata('');
                ?>
               <div class="x_content">
                <!-- ==============   Form   ================ -->
                  <form class="form-horizontal form-label-left" method="post" action="<?php echo base_url().'add_id'; ?>" novalidate enctype="multipart/form-data">
                     <span class="section"><?php echo $heading; ?></span>
                     <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="store_name">Store Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <input id="store_name" class="form-control col-md-7 col-xs-12" name="store_name" required="required" type="text">
                        </div>
                     </div>
                     <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="store_id">Store ID <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <input id="store_id" class="form-control col-md-7 col-xs-12" name="store_id" required="required" type="number">
                        </div>
                     </div>
                     <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="profile_id">Profile ID <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                           <input type="text" id="profile_id" name="profile_id" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                     </div>
                     <div class="ln_solid"></div>
                     <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                           <a href="<?php echo base_url(); ?>register" class="btn btn-primary">Cancel</a>
                           <input id="send" type="submit" name="submit" class="btn btn-success" value="Submit">
                        </div>
                     </div>
                  </form>
                  <!-- ==============   End of Form   ================ -->
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- /page content -->