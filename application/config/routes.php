<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
*/
$route['default_controller'] = 'api/loginPage';

$route['dashboard'] 	= 	'api';
$route['add_id'] 		= 	'api/add_new_profile_id';
$route['profiles'] 		= 	'api/all_profile_records';
$route['delete/(:any)'] = 	'api/delete_fuct/$1';
$route['edit/(:any)'] 	= 	'api/view_update_fuct/$1';
$route['update/(:any)'] = 	'api/update_funct/$1';
$route['logs'] 			= 	'api/show_logs_page';
$route['login'] 		= 	'api/loginPage';
$route['logout'] 		= 	'api/logout_User';




$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
