<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	public function __construct() {
		parent::__construct(); 
		$this->load->library('form_validation');
		$this->load->model('Auth');
	}

	public function index() {
		$data['title'] = "Dashboard";
		$data['heading'] = "Add New Profile ID From.";

		$this->load->view('includes/_header', $data);
		$this->load->view('includes/_sidebar');
		$this->load->view('dashboard', $data);
		$this->load->view('includes/_footer');
	}


	public function get_post_data(){
		//==================== Craete and Write a File ================
		 // $path 			= 		$this->config->_config_paths[0].'log_files';
		 // $time_stamp		=		date('d-m-Y_s', time());
		 // $my_file 		= 		'log_'.$time_stamp.'.txt';
		 // $handle 		= 		fopen($path.'/'.$my_file, 'w');
		 // $data 			= 		$_POST;

		 // fwrite($handle, var_export($data, true));


		// echo "<pre>";
		// print_r($this->static_array());
		// echo "</pre>";


		// $hook_data = $this->static_array();


		
		$hook_data 	= 	$_POST;

		//=========== Getting Last array key name ==========
		$arrayKeys 	= 	array_keys($hook_data);
		$endData 	= 	end($arrayKeys); 
		$lastRow 	=  	explode('_',$endData); 
		$lastRow 	=  	end($lastRow);
		
	
		$counter 	= 	1;

        $finalArray = 	array();

		foreach ($hook_data as $key => $value) {

			for ($i=0; $i <= 6; $i++) { 

				if($counter > 1 && $counter <= $lastRow){
					$store_id 	= "how_are_we_doing_0_".$counter;
					$email 		= "how_are_we_doing_2_".$counter;
					$name 		= "how_are_we_doing_3_".$counter;
					$rating 	= "how_are_we_doing_6_".$counter;

           			$store_id  =  	$hook_data["$store_id"];
           			$email 	   = 	$hook_data["$email"];
           			$name 	   = 	$hook_data["$name"];
           			$rating 	= 	$hook_data["$rating"];

           			$finalArray[$store_id][] = array(
           			   "email_address" 	=>	$email ,
           			   "phone_number" 	=>	'',
           			   "first_name" 	=>	$name,
           			   "last_name" 		=>	"",
           			   "rating"			=>	$rating
           			);
               	}

				$counter++;
			}
		}

		//======== API Token ============
		$api_token 		= 	"9e51e0503bfb45b5b38b5bcae6167911";




		//===================== List of Store ID Recepients ======================
		$recipients['recipients'] = array();
		foreach ($finalArray as $val_key => $ListRecipients) {
			foreach ($ListRecipients as $val) {
				if($val_key>0){
					$recipients['recipients'][]=$val;
				}
			}
			if(!empty($recipients['recipients'])){

				$profileID_data 	= 	$this->Auth->get_profile_id('tbl_product_ids', $val_key);
				$profile_id 		= 	$profileID_data['profile_id'];
				$url 				= 	'https://www.grade.us/api/v2/profiles/'.$profile_id.'/recipients';
				$response 			= 	$this->runCurl($api_token, $url, $recipients);

				$this->insert_local_and_curl($profile_id, $val_key, $response, $hook_data, $recipients);
			}

		}		





		//==================== List of Getting Non StoreID Recepients ================
		foreach ($finalArray as $key => $recipientsList) {
            
            $recipients_less3['recipients'] = array();
            $recipients_greater3['recipients'] = array();

            $i = $j = $m = 0;
			
			foreach ($recipientsList as  $recUser) {
	             
            	if($recUser['rating'] < 3.0 && $key == 0){

            		$recipients_less3['recipients'][$i]['email_address'] = $recUser['email_address'];
            		$recipients_less3['recipients'][$i]['phone_number'] = $recUser['phone_number'];
            		$recipients_less3['recipients'][$i]['first_name'] = $recUser['first_name'];
            		$recipients_less3['recipients'][$i]['last_name'] = $recUser['last_name']; 
 					$i++;
             
            	} elseif($recUser['rating'] >= 3.0 && $key == 0){

            		$recipients_greater3['recipients'][$m]['email_address'] = $recUser['email_address'];
            		$recipients_greater3['recipients'][$m]['phone_number'] = $recUser['phone_number'];
            		$recipients_greater3['recipients'][$m]['first_name'] = $recUser['first_name'];
            		$recipients_greater3['recipients'][$m]['last_name'] = $recUser['last_name'];
 					$m++;
            	}     

			}	 




	
			//============== Uncle Julio's Email Only (For rating < 3.0) ============
			if(!empty($recipients_less3['recipients'])){

				$profile_id 	= 	'd8f876e6-5752-471a-b02e-23ee9b8abd09';
				$url 			= 	'https://www.grade.us/api/v2/profiles/'.$profile_id.'/recipients';
				$response 		= 	$this->runCurl($api_token, $url, $recipients_less3);
				$recipients 	= 	$recipients_less3;
				$key 			= 	'';

				$this->insert_local_and_curl($profile_id, $key, $response, $hook_data, $recipients);
			} 




			//==============  Uncle Julios Loyalty (For rating > 2.9) ==================
			if(!empty($recipients_greater3['recipients'])){

				$profile_id 	= 	'50cea2f7-c001-4789-a4fb-14b541c18546';
				$url 			= 	'https://www.grade.us/api/v2/profiles/'.$profile_id.'/recipients';
				$response 		= 	$this->runCurl($api_token, $url, $recipients_greater3);
				$recipients 	= 	$recipients_greater3;
				$key 			= 	'';

				$this->insert_local_and_curl($profile_id, $key, $response, $hook_data, $recipients);
			}

		}

	}


	//========== Function insert data usign CURL =============
	function insert_local_and_curl($profile_id, $key, $response, $hook_data, $recipients){

			$log_data = array();

			if($profile_id){

			if($response['recipients']) {
				
				$log_data['processed_status'] 	= 	'Success';
				$log_data['response_data'] 		= 	json_encode($response);

			} elseif(!empty($response['meta']['errors'])){

				$log_data['processed_status'] 	= 	'Failed';
				$log_data['response_data'] 		= 	json_encode($response);
			}
			//------  data for log table enrty -------
			$log_data['store_id'] 			= 	$key;
			$log_data['profile_id'] 		= 	$profile_id;
			$log_data['process_on'] 		= 	$hook_data['processed_at'];
			$log_data['request_data'] 		= 	json_encode($recipients); 

			$this->Auth->insert_function('tbl_logs', $log_data);

		} else{
			//------  data for log table enrty -------
			$log_data['store_id'] 			= 	$key;
			$log_data['profile_id'] 		= 	'not found';
			$log_data['process_on'] 		= 	$hook_data['processed_at'];
			$log_data['processed_status'] 	= 	'not found';
			$log_data['request_data'] 		= 	json_encode($recipients); 
			$log_data['response_data'] 		= 	json_encode($response);

			$this->Auth->insert_function('tbl_logs', $log_data);
		}
	}




	//=========== Function Run Curl Script ==========
	public function runCurl($api_token, $url, $request_data ){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		  'Content-Type: application/json', 
		  'Authorization: Token '.$api_token
		)); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_POST,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request_data));
		curl_setopt($ch, CURLOPT_URL, $url);
		
		return $response = json_decode(curl_exec($ch), true);
		curl_close($ch); 
	}





	//============ Add new Profile ID Function ==========

	public function add_new_profile_id(){
		if ($this->input->post('submit')) {
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
			$this->form_validation->set_rules('profile_id', 'Profile ID', 'required');
			$this->form_validation->set_rules('store_name', 'Store Name', 'required');
			$this->form_validation->set_rules('store_id', 'Store ID', 'required|is_unique[tbl_product_ids.	store_id]', array('is_unique'=> 'This Store ID already exist'));

			if($this->form_validation->run() == FALSE) {
		      $this->index();
		    } else {
		    	$data = $_POST;
		    	unset($data['submit']);

		    	$user_data = $this->Auth->insert_function('tbl_product_ids', $data);
		    	if($user_data){
					$this->session->set_flashdata('success', "New Profile ID added Successfully!");
					$this->index();
				} else {
					$this->session->set_flashdata('error', "Oh Sorry! there is something going on wrong.");
				}
		    }
		}
		
	}


	//=============== View All Profile ID's From Table function ==============

	public function all_profile_records() {
		$table_data = $this->Auth->get_data('tbl_product_ids');
		$data['title'] = "View Profiles";
		$data['heading'] = "List of Added Profiles";
		$data['table_data'] =  $table_data;
			$this->load->view('includes/_header', $data);
			$this->load->view('includes/_sidebar');
			$this->load->view('view_profileIDs', $data);
			$this->load->view('includes/_footer');
	}





	//================ Delete entry from table ==============

	public function delete_fuct($id){
		$data = $this->Auth->delt_function('tbl_product_ids', $id);

		if($data > 0){
			$this->session->set_flashdata('success', "Profile ID deleted Successfully!");
			redirect(base_url().'profiles', 'refresh');
		} else {
			$this->session->set_flashdata('error', "Sorry no data found to update.");
			redirect(base_url().'profiles', 'refresh');
		}

	}


	//================ Edit Table record ==============
	public function view_update_fuct($id){

		$table_data = $this->Auth->get_current_user('tbl_product_ids', $id);;
		$data['title'] = "Edit Profiles";
		$data['heading'] = "Edit Profiles ID";
		$data['table_data'] =  $table_data;


		$this->load->view('includes/_header', $data);
		$this->load->view('includes/_sidebar');
		$this->load->view('edit-profile_id', $data);
		$this->load->view('includes/_footer');
	}





	//==============  Update exist table record ============
	public function update_funct($id){

		array_pop($_POST);
		$data = $this->Auth->update_function('tbl_product_ids', $_POST, $id);

		if($data > 0){
			$this->session->set_flashdata('success', "Updated Successfully!");
			redirect(base_url().'edit/'.$id, 'refresh');
		} else {
			$this->session->set_flashdata('error', "Sorry no data found to update.");
			redirect(base_url().'edit/'.$id, 'refresh');
		}
	}



	//=============  Function view logs record page ==========
	function show_logs_page(){
		$table_data = $this->Auth->get_data('tbl_logs');
		$data['title'] = "Logs";
		$data['heading'] = "View all Logs";
		$data['table_data'] =  $table_data;

		//if ($this->session->login) {
		$this->load->view('includes/_header', $data);
		$this->load->view('includes/_sidebar');
		$this->load->view('view_logs', $data);
		$this->load->view('includes/_footer');
	}


	//============= Function to view front login page ==============
	function loginPage(){
		if ($this->session->login) {
			redirect(base_url().'dashboard', 'refresh');
		} else{
			$data['title'] = "Login";
			$data['heading'] = "Login";
			$this->load->view('login', $data);
		}	
	}


	//============== User Login Function ================
	public function user_login(){
		if ($this->input->post('login')) {
			unset($_POST['login']);
			$_POST['password'] = md5($this->input->post('password'));
			$data =  $this->Auth->check_login($_POST, 'tbl_users');
				if($data){
					$_SESSION['login'] = $data;
					redirect(base_url().'dashboard');		
				} else {
					$this->session->set_flashdata('error', "Sorry! Invalid User.");
					redirect(base_url().'login', 'refresh');
				}
		}

	}


	//=========== User Logout Function =============
	public function logout_User(){
		unset(
			$_SESSION['login']
		);
		$this->session->sess_destroy();
		$this->session->set_flashdata('success', "User logout Successfully!.");
		redirect(base_url(), 'refresh');
	}


	//=========== User Logout Function =============
	public function execute_curl($url,$request_data,$api_token){
		$ch = curl_init();
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			  'Content-Type: application/json', 
			  'Authorization: Token '.$api_token
			)); 
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_POST,1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request_data));
			curl_setopt($ch, CURLOPT_URL, 'https://www.grade.us/api/v2/profiles/0bc1ad00-d5ea-44a6-8d35-988f36ba8fa1/recipients');
			
			return $response = json_decode(curl_exec($ch), true); 

	}


	function static_array(){
		return array (
					  'id' => '4e68a5a21cc6c50598b5d35fbfff8f80',
					  'received_at' => '2019-05-30 15:45:13',
					  'received_at_timestamp' => '1559231113',
					  'received_at_iso8601' => '2019-05-30T15:45:13+00:00',
					  'processed_at' => '2019-05-30 15:46:09',
					  'processed_at_timestamp' => '1559231169',
					  'processed_at_iso8601' => '2019-05-30T15:46:09+00:00',
					  'how_are_we_doing_0' => 'xStoreID',
					  'how_are_we_doing_1' => 'xExtractDate',
					  'how_are_we_doing_2' => 'EmailAddress',
					  'how_are_we_doing_3' => 'TenderName',
					  'how_are_we_doing_4' => 'Location',
					  'how_are_we_doing_5' => 'GeneralManager',
					  'how_are_we_doing_6' => 'AvgPoints',
					  'how_are_we_doing_0_2' => '',
					  'how_are_we_doing_1_2' => '29/05/2019',
					  'how_are_we_doing_2_2' => 'adam.j.janca@ehi.com',
					  'how_are_we_doing_3_2' => 'JANCA ADAM J',
					  'how_are_we_doing_4_2' => 'Uncle Julio\'s Dallas',
					  'how_are_we_doing_5_2' => 'Cliff Hodge',
					  'how_are_we_doing_6_2' => '2.0',
					  'how_are_we_doing_0_3' => '1',
					  'how_are_we_doing_1_3' => '29/05/2019',
					  'how_are_we_doing_2_3' => 'c.heslep@att.net',
					  'how_are_we_doing_3_3' => 'MARTIN ROBERT',
					  'how_are_we_doing_4_3' => 'Uncle Julio\'s Dallas',
					  'how_are_we_doing_5_3' => 'Cliff Hodge',
					  'how_are_we_doing_6_3' => '4.0',
					  'how_are_we_doing_0_4' => '',
					  'how_are_we_doing_1_4' => '29/05/2019',
					  'how_are_we_doing_2_4' => 'chris.logan@gulfstream.com',
					  'how_are_we_doing_3_4' => 'LOGAN CHRISTOPHER K',
					  'how_are_we_doing_4_4' => 'Uncle Julio\'s Dallas',
					  'how_are_we_doing_5_4' => 'Cliff Hodge',
					  'how_are_we_doing_6_4' => '4.0',
					  'how_are_we_doing_0_5' => '',
					  'how_are_we_doing_1_5' => '29/05/2019',
					  'how_are_we_doing_2_5' => 'cordellsimmons2012@gmail.com',
					  'how_are_we_doing_3_5' => 'SIMMONS  CORDELL',
					  'how_are_we_doing_4_5' => 'Uncle Julio\'s Dallas',
					  'how_are_we_doing_5_5' => 'Cliff Hodge',
					  'how_are_we_doing_6_5' => '4.0',
					  'how_are_we_doing_0_6' => '1',
					  'how_are_we_doing_1_6' => '29/05/2019',
					  'how_are_we_doing_2_6' => 'dalmarsh103@gmail.com',
					  'how_are_we_doing_3_6' => 'MOREAU STEVE M',
					  'how_are_we_doing_4_6' => 'Uncle Julio\'s Dallas',
					  'how_are_we_doing_5_6' => 'Cliff Hodge',
					  'how_are_we_doing_6_6' => '4.0',
					  'how_are_we_doing_0_7' => '',
					  'how_are_we_doing_1_7' => '29/05/2019',
					  'how_are_we_doing_2_7' => 'dchen2@mac.com',
					  'how_are_we_doing_3_7' => 'CHEN DAVID',
					  'how_are_we_doing_4_7' => 'Uncle Julio\'s Dallas',
					  'how_are_we_doing_5_7' => 'Cliff Hodge',
					  'how_are_we_doing_6_7' => '3.0',
					  'how_are_we_doing_0_8' => '1',
					  'how_are_we_doing_1_8' => '29/05/2019',
					  'how_are_we_doing_2_8' => 'deu1960@gmail.com',
					  'how_are_we_doing_3_8' => 'UECKERT DOUGLAS E',
					  'how_are_we_doing_4_8' => 'Uncle Julio\'s Dallas',
					  'how_are_we_doing_5_8' => 'Cliff Hodge',
					  'how_are_we_doing_6_8' => '3.0',
					  'how_are_we_doing_0_9' => '1',
					  'how_are_we_doing_1_9' => '29/05/2019',
					  'how_are_we_doing_2_9' => 'donmozee12@gmail.com',
					  'how_are_we_doing_3_9' => 'ALANIZ SHANNON RENEE',
					  'how_are_we_doing_4_9' => 'Uncle Julio\'s Dallas',
					  'how_are_we_doing_5_9' => 'Cliff Hodge',
					  'how_are_we_doing_6_9' => '4.0',
					  'how_are_we_doing_0_10' => '',
					  'how_are_we_doing_1_10' => '29/05/2019',
					  'how_are_we_doing_2_10' => 'dwh1031@yahoo.com',
					  'how_are_we_doing_3_10' => 'HARRISON DAVID',
					  'how_are_we_doing_4_10' => 'Uncle Julio\'s Dallas',
					  'how_are_we_doing_5_10' => 'Cliff Hodge',
					  'how_are_we_doing_6_10' => '4.0',
					  'how_are_we_doing_0_11' => '1',
					  'how_are_we_doing_1_11' => '29/05/2019',
					  'how_are_we_doing_2_11' => 'e@eddieyarmer.com',
					  'how_are_we_doing_3_11' => 'YARMER CLYDE',
					  'how_are_we_doing_4_11' => 'Uncle Julio\'s Dallas',
					  'how_are_we_doing_5_11' => 'Cliff Hodge',
					  'how_are_we_doing_6_11' => '2.0',
					  'how_are_we_doing_0_12' => '1',
					  'how_are_we_doing_1_12' => '29/05/2019',
					  'how_are_we_doing_2_12' => 'gatesgirlgg@yahoo.com',
					  'how_are_we_doing_3_12' => 'GATES JAMES E',
					  'how_are_we_doing_4_12' => 'Uncle Julio\'s Dallas',
					  'how_are_we_doing_5_12' => 'Cliff Hodge',
					  'how_are_we_doing_6_12' => '3.0',
					  'how_are_we_doing_0_13' => '',
					  'how_are_we_doing_1_13' => '29/05/2019',
					  'how_are_we_doing_2_13' => 'joie@villagesalons.com',
					  'how_are_we_doing_3_13' => 'BUIE JOLIE M',
					  'how_are_we_doing_4_13' => 'Uncle Julio\'s Dallas',
					  'how_are_we_doing_5_13' => 'Cliff Hodge',
					  'how_are_we_doing_6_13' => '4.0',
					  'how_are_we_doing_0_14' => '1',
					  'how_are_we_doing_1_14' => '29/05/2019',
					  'how_are_we_doing_2_14' => 'jshawsmu@gmail.com',
					  'how_are_we_doing_3_14' => 'SHAW JASON L',
					  'how_are_we_doing_4_14' => 'Uncle Julio\'s Dallas',
					  'how_are_we_doing_5_14' => 'Cliff Hodge',
					  'how_are_we_doing_6_14' => '3.0',
					  'how_are_we_doing_0_15' => '1',
					  'how_are_we_doing_1_15' => '29/05/2019',
					  'how_are_we_doing_2_15' => 'kimberwalt@gmail.com',
					  'how_are_we_doing_3_15' => 'WALTERS-SANDER KIMBERLEE',
					  'how_are_we_doing_4_15' => 'Uncle Julio\'s Dallas',
					  'how_are_we_doing_5_15' => 'Cliff Hodge',
					  'how_are_we_doing_6_15' => '4.0',
					  'how_are_we_doing_0_16' => '',
					  'how_are_we_doing_1_16' => '29/05/2019',
					  'how_are_we_doing_2_16' => 'liatepker@yahoo.com',
					  'how_are_we_doing_3_16' => 'MCHUGHES LIA',
					  'how_are_we_doing_4_16' => 'Uncle Julio\'s Dallas',
					  'how_are_we_doing_5_16' => 'Cliff Hodge',
					  'how_are_we_doing_6_16' => '2.6',
					  'how_are_we_doing_0_17' => '1',
					  'how_are_we_doing_1_17' => '29/05/2019',
					  'how_are_we_doing_2_17' => 'lilsteph00@att.net',
					  'how_are_we_doing_3_17' => 'WARE STEPHANIE D',
					  'how_are_we_doing_4_17' => 'Uncle Julio\'s Dallas',
					  'how_are_we_doing_5_17' => 'Cliff Hodge',
					  'how_are_we_doing_6_17' => '4.0',
					  'how_are_we_doing_0_18' => '1',
					  'how_are_we_doing_1_18' => '29/05/2019',
					  'how_are_we_doing_2_18' => 'linda.berlanga@wnco.com',
					  'how_are_we_doing_3_18' => 'BERLANGA LINDA',
					  'how_are_we_doing_4_18' => 'Uncle Julio\'s Dallas',
					  'how_are_we_doing_5_18' => 'Cliff Hodge',
					  'how_are_we_doing_6_18' => '4.0',
					  'how_are_we_doing_0_19' => '1',
					  'how_are_we_doing_1_19' => '29/05/2019',
					  'how_are_we_doing_2_19' => 'lmcferrin@gmail.com',
					  'how_are_we_doing_3_19' => 'MCFERRIN HOGAN LAURA',
					  'how_are_we_doing_4_19' => 'Uncle Julio\'s Dallas',
					  'how_are_we_doing_5_19' => 'Cliff Hodge',
					  'how_are_we_doing_6_19' => '4.0',
					  'how_are_we_doing_0_20' => '1',
					  'how_are_we_doing_1_20' => '29/05/2019',
					  'how_are_we_doing_2_20' => 'marcink7@gmail.com',
					  'how_are_we_doing_3_20' => 'KEDZIERSKI MARCIN',
					  'how_are_we_doing_4_20' => 'Uncle Julio\'s Dallas',
					  'how_are_we_doing_5_20' => 'Cliff Hodge',
					  'how_are_we_doing_6_20' => '4.0',
					  'how_are_we_doing_0_21' => '1',
					  'how_are_we_doing_1_21' => '29/05/2019',
					  'how_are_we_doing_2_21' => 'michael.kelso@jetaviation.com',
					  'how_are_we_doing_3_21' => 'KELSO MICHAEL O',
					  'how_are_we_doing_4_21' => 'Uncle Julio\'s Dallas',
					  'how_are_we_doing_5_21' => 'Cliff Hodge',
					  'how_are_we_doing_6_21' => '4.0',
					  'how_are_we_doing_0_22' => '1',
					  'how_are_we_doing_1_22' => '29/05/2019',
					  'how_are_we_doing_2_22' => 'mlittle@nuvistaonline.com',
					  'how_are_we_doing_3_22' => 'LITTLE MATTHEW',
					  'how_are_we_doing_4_22' => 'Uncle Julio\'s Dallas',
					  'how_are_we_doing_5_22' => 'Cliff Hodge',
					  'how_are_we_doing_6_22' => '3.0',
					  'how_are_we_doing_0_23' => '1',
					  'how_are_we_doing_1_23' => '29/05/2019',
					  'how_are_we_doing_2_23' => 'nixonp1504@gmail.com',
					  'how_are_we_doing_3_23' => 'NIXON PATRICIA M',
					  'how_are_we_doing_4_23' => 'Uncle Julio\'s Dallas',
					  'how_are_we_doing_5_23' => 'Cliff Hodge',
					  'how_are_we_doing_6_23' => '4.0',
					  'how_are_we_doing_0_24' => '1',
					  'how_are_we_doing_1_24' => '29/05/2019',
					  'how_are_we_doing_2_24' => 'pathum08@yahoo.com',
					  'how_are_we_doing_3_24' => 'KALITU-ARACHCHIGE PATHUM',
					  'how_are_we_doing_4_24' => 'Uncle Julio\'s Dallas',
					  'how_are_we_doing_5_24' => 'Cliff Hodge',
					  'how_are_we_doing_6_24' => '4.0',
					  'how_are_we_doing_0_25' => '1',
					  'how_are_we_doing_1_25' => '29/05/2019',
					  'how_are_we_doing_2_25' => 'rdz123017@gmail.com',
					  'how_are_we_doing_3_25' => 'RODRIGUEZ LISA',
					  'how_are_we_doing_4_25' => 'Uncle Julio\'s Dallas',
					  'how_are_we_doing_5_25' => 'Cliff Hodge',
					  'how_are_we_doing_6_25' => '3.0',
					  'how_are_we_doing_0_26' => '',
					  'how_are_we_doing_1_26' => '29/05/2019',
					  'how_are_we_doing_2_26' => 'sjyjam47@yahoo.com',
					  'how_are_we_doing_3_26' => 'Alex Martin',
					  'how_are_we_doing_4_26' => 'Uncle Julio\'s Dallas',
					  'how_are_we_doing_5_26' => 'Cliff Hodge',
					  'how_are_we_doing_6_26' => '3.5',
					  'how_are_we_doing_0_27' => '1',
					  'how_are_we_doing_1_27' => '29/05/2019',
					  'how_are_we_doing_2_27' => 'taylivingston11@gmail.com',
					  'how_are_we_doing_3_27' => 'LIVINGSTON TAYLOR B',
					  'how_are_we_doing_4_27' => 'Uncle Julio\'s Dallas',
					  'how_are_we_doing_5_27' => 'Cliff Hodge',
					  'how_are_we_doing_6_27' => '4.0',
					  'how_are_we_doing_0_28' => '1',
					  'how_are_we_doing_1_28' => '29/05/2019',
					  'how_are_we_doing_2_28' => 'vescontrias@yahoo.com',
					  'how_are_we_doing_3_28' => 'WHITENER KEVIN A',
					  'how_are_we_doing_4_28' => 'Uncle Julio\'s Dallas',
					  'how_are_we_doing_5_28' => 'Cliff Hodge',
					  'how_are_we_doing_6_28' => '4.0',
					  'how_are_we_doing_0_29' => '1',
					  'how_are_we_doing_1_29' => '29/05/2019',
					  'how_are_we_doing_2_29' => 'william.liu129@gmail.com',
					  'how_are_we_doing_3_29' => 'LIU WILLIAM',
					  'how_are_we_doing_4_29' => 'Uncle Julio\'s Dallas',
					  'how_are_we_doing_5_29' => 'Cliff Hodge',
					  'how_are_we_doing_6_29' => '3.7',
					  'how_are_we_doing_0_30' => '2',
					  'how_are_we_doing_1_30' => '29/05/2019',
					  'how_are_we_doing_2_30' => 'abutler@powerdesigninc.us',
					  'how_are_we_doing_3_30' => 'ROBERTS KEITH',
					  'how_are_we_doing_4_30' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_30' => 'Justin Knight',
					  'how_are_we_doing_6_30' => '4.0',
					  'how_are_we_doing_0_31' => '2',
					  'how_are_we_doing_1_31' => '29/05/2019',
					  'how_are_we_doing_2_31' => 'ariella.brodecki@gmail.com',
					  'how_are_we_doing_3_31' => 'BRODECKI ARIELLA E',
					  'how_are_we_doing_4_31' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_31' => 'Justin Knight',
					  'how_are_we_doing_6_31' => '4.0',
					  'how_are_we_doing_0_32' => '2',
					  'how_are_we_doing_1_32' => '29/05/2019',
					  'how_are_we_doing_2_32' => 'birhanumuna@yahoo.com',
					  'how_are_we_doing_3_32' => 'YOHANES  MUNA',
					  'how_are_we_doing_4_32' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_32' => 'Justin Knight',
					  'how_are_we_doing_6_32' => '3.0',
					  'how_are_we_doing_0_33' => '2',
					  'how_are_we_doing_1_33' => '29/05/2019',
					  'how_are_we_doing_2_33' => 'brandonbarr650@gmail.com',
					  'how_are_we_doing_3_33' => 'BARR BRANDON J',
					  'how_are_we_doing_4_33' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_33' => 'Justin Knight',
					  'how_are_we_doing_6_33' => '4.0',
					  'how_are_we_doing_0_34' => '2',
					  'how_are_we_doing_1_34' => '29/05/2019',
					  'how_are_we_doing_2_34' => 'coueda@hotmail.com',
					  'how_are_we_doing_3_34' => 'OUEDACRUZ LUCIE C',
					  'how_are_we_doing_4_34' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_34' => 'Justin Knight',
					  'how_are_we_doing_6_34' => '4.0',
					  'how_are_we_doing_0_35' => '2',
					  'how_are_we_doing_1_35' => '29/05/2019',
					  'how_are_we_doing_2_35' => 'crx687@gmail.com',
					  'how_are_we_doing_3_35' => 'XIAO CHANGRUI',
					  'how_are_we_doing_4_35' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_35' => 'Justin Knight',
					  'how_are_we_doing_6_35' => '4.0',
					  'how_are_we_doing_0_36' => '2',
					  'how_are_we_doing_1_36' => '29/05/2019',
					  'how_are_we_doing_2_36' => 'daniella.poppovici@gmail.com',
					  'how_are_we_doing_3_36' => 'POPOVICI DANIELLA',
					  'how_are_we_doing_4_36' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_36' => 'Justin Knight',
					  'how_are_we_doing_6_36' => '3.0',
					  'how_are_we_doing_0_37' => '2',
					  'how_are_we_doing_1_37' => '29/05/2019',
					  'how_are_we_doing_2_37' => 'dothkorel@gmail.com',
					  'how_are_we_doing_3_37' => 'CATHELINEAUD RAFAEL E',
					  'how_are_we_doing_4_37' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_37' => 'Justin Knight',
					  'how_are_we_doing_6_37' => '4.0',
					  'how_are_we_doing_0_38' => '2',
					  'how_are_we_doing_1_38' => '29/05/2019',
					  'how_are_we_doing_2_38' => 'dowlerj@bellsouth.net',
					  'how_are_we_doing_3_38' => 'DOWLER JULIE',
					  'how_are_we_doing_4_38' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_38' => 'Justin Knight',
					  'how_are_we_doing_6_38' => '4.0',
					  'how_are_we_doing_0_39' => '2',
					  'how_are_we_doing_1_39' => '29/05/2019',
					  'how_are_we_doing_2_39' => 'drhodor@gmail.com',
					  'how_are_we_doing_3_39' => 'HODOR JONATHAN G',
					  'how_are_we_doing_4_39' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_39' => 'Justin Knight',
					  'how_are_we_doing_6_39' => '3.0',
					  'how_are_we_doing_0_40' => '2',
					  'how_are_we_doing_1_40' => '29/05/2019',
					  'how_are_we_doing_2_40' => 'f6ross@gmail.com',
					  'how_are_we_doing_3_40' => 'ROSSIGNOL FRANCIS',
					  'how_are_we_doing_4_40' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_40' => 'Justin Knight',
					  'how_are_we_doing_6_40' => '3.0',
					  'how_are_we_doing_0_41' => '2',
					  'how_are_we_doing_1_41' => '29/05/2019',
					  'how_are_we_doing_2_41' => 'gary777@msn.com',
					  'how_are_we_doing_3_41' => 'JACOBS GARY',
					  'how_are_we_doing_4_41' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_41' => 'Justin Knight',
					  'how_are_we_doing_6_41' => '3.0',
					  'how_are_we_doing_0_42' => '2',
					  'how_are_we_doing_1_42' => '29/05/2019',
					  'how_are_we_doing_2_42' => 'irisp2248@gmail.com',
					  'how_are_we_doing_3_42' => 'PEREZ IRIS Y',
					  'how_are_we_doing_4_42' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_42' => 'Justin Knight',
					  'how_are_we_doing_6_42' => '3.0',
					  'how_are_we_doing_0_43' => '2',
					  'how_are_we_doing_1_43' => '29/05/2019',
					  'how_are_we_doing_2_43' => 'james.e.dameron@gmail.com',
					  'how_are_we_doing_3_43' => 'DAMERON  JAMES',
					  'how_are_we_doing_4_43' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_43' => 'Justin Knight',
					  'how_are_we_doing_6_43' => '4.0',
					  'how_are_we_doing_0_44' => '2',
					  'how_are_we_doing_1_44' => '29/05/2019',
					  'how_are_we_doing_2_44' => 'jbackenstoe@icloud.com',
					  'how_are_we_doing_3_44' => 'BACKENSTOE GERALD J',
					  'how_are_we_doing_4_44' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_44' => 'Justin Knight',
					  'how_are_we_doing_6_44' => '4.0',
					  'how_are_we_doing_0_45' => '2',
					  'how_are_we_doing_1_45' => '29/05/2019',
					  'how_are_we_doing_2_45' => 'johnrfennig@yahoo.com',
					  'how_are_we_doing_3_45' => 'FENNIG JOHN R',
					  'how_are_we_doing_4_45' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_45' => 'Justin Knight',
					  'how_are_we_doing_6_45' => '3.0',
					  'how_are_we_doing_0_46' => '2',
					  'how_are_we_doing_1_46' => '29/05/2019',
					  'how_are_we_doing_2_46' => 'jraffertyi@aol.com',
					  'how_are_we_doing_3_46' => 'RAFFERTY  JOSEPH',
					  'how_are_we_doing_4_46' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_46' => 'Justin Knight',
					  'how_are_we_doing_6_46' => '4.0',
					  'how_are_we_doing_0_47' => '2',
					  'how_are_we_doing_1_47' => '29/05/2019',
					  'how_are_we_doing_2_47' => 'julcole@gmail.com',
					  'how_are_we_doing_3_47' => 'HAMMONS JULIE',
					  'how_are_we_doing_4_47' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_47' => 'Justin Knight',
					  'how_are_we_doing_6_47' => '4.0',
					  'how_are_we_doing_0_48' => '2',
					  'how_are_we_doing_1_48' => '29/05/2019',
					  'how_are_we_doing_2_48' => 'kathyrmoran@yahoo.com',
					  'how_are_we_doing_3_48' => 'MORAN KATHLEEN A',
					  'how_are_we_doing_4_48' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_48' => 'Justin Knight',
					  'how_are_we_doing_6_48' => '4.0',
					  'how_are_we_doing_0_49' => '2',
					  'how_are_we_doing_1_49' => '29/05/2019',
					  'how_are_we_doing_2_49' => 'kundanmal.ck@gmail.com',
					  'how_are_we_doing_3_49' => 'KUNDANMAL ADRIAN',
					  'how_are_we_doing_4_49' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_49' => 'Justin Knight',
					  'how_are_we_doing_6_49' => '4.0',
					  'how_are_we_doing_0_50' => '2',
					  'how_are_we_doing_1_50' => '29/05/2019',
					  'how_are_we_doing_2_50' => 'lesliegreenberg5@gmail.com',
					  'how_are_we_doing_3_50' => 'GREENBERG LESLIE',
					  'how_are_we_doing_4_50' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_50' => 'Justin Knight',
					  'how_are_we_doing_6_50' => '3.0',
					  'how_are_we_doing_0_51' => '2',
					  'how_are_we_doing_1_51' => '29/05/2019',
					  'how_are_we_doing_2_51' => 'lexisland108@gmail.com',
					  'how_are_we_doing_3_51' => 'FUHRMAN ALEXIS',
					  'how_are_we_doing_4_51' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_51' => 'Justin Knight',
					  'how_are_we_doing_6_51' => '2.3',
					  'how_are_we_doing_0_52' => '2',
					  'how_are_we_doing_1_52' => '29/05/2019',
					  'how_are_we_doing_2_52' => 'mauricio_123177@hotmail.com',
					  'how_are_we_doing_3_52' => '',
					  'how_are_we_doing_4_52' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_52' => 'Justin Knight',
					  'how_are_we_doing_6_52' => '3.0',
					  'how_are_we_doing_0_53' => '2',
					  'how_are_we_doing_1_53' => '29/05/2019',
					  'how_are_we_doing_2_53' => 'mbzstokes@gmail.com',
					  'how_are_we_doing_3_53' => 'ZIMMERMAN MERIDITH B',
					  'how_are_we_doing_4_53' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_53' => 'Justin Knight',
					  'how_are_we_doing_6_53' => '3.0',
					  'how_are_we_doing_0_54' => '2',
					  'how_are_we_doing_1_54' => '29/05/2019',
					  'how_are_we_doing_2_54' => 'mfalkenrath@yahoo.com',
					  'how_are_we_doing_3_54' => 'FALKENRATH MICHAEL',
					  'how_are_we_doing_4_54' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_54' => 'Justin Knight',
					  'how_are_we_doing_6_54' => '4.0',
					  'how_are_we_doing_0_55' => '2',
					  'how_are_we_doing_1_55' => '29/05/2019',
					  'how_are_we_doing_2_55' => 'msangeorge@gmail.com',
					  'how_are_we_doing_3_55' => 'SANGEORGE MARISSA E',
					  'how_are_we_doing_4_55' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_55' => 'Justin Knight',
					  'how_are_we_doing_6_55' => '3.0',
					  'how_are_we_doing_0_56' => '2',
					  'how_are_we_doing_1_56' => '29/05/2019',
					  'how_are_we_doing_2_56' => 'msarro19@gmail.com',
					  'how_are_we_doing_3_56' => 'SARRO MATTHEW',
					  'how_are_we_doing_4_56' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_56' => 'Justin Knight',
					  'how_are_we_doing_6_56' => '4.0',
					  'how_are_we_doing_0_57' => '2',
					  'how_are_we_doing_1_57' => '29/05/2019',
					  'how_are_we_doing_2_57' => 'nadiralsalam@hotmail.com',
					  'how_are_we_doing_3_57' => 'AL SALAM NADIR',
					  'how_are_we_doing_4_57' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_57' => 'Justin Knight',
					  'how_are_we_doing_6_57' => '4.0',
					  'how_are_we_doing_0_58' => '2',
					  'how_are_we_doing_1_58' => '29/05/2019',
					  'how_are_we_doing_2_58' => 'pfberman@aol.com',
					  'how_are_we_doing_3_58' => 'DEITSCH JACQUALINE',
					  'how_are_we_doing_4_58' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_58' => 'Justin Knight',
					  'how_are_we_doing_6_58' => '3.0',
					  'how_are_we_doing_0_59' => '2',
					  'how_are_we_doing_1_59' => '29/05/2019',
					  'how_are_we_doing_2_59' => 'prissmissaka@hotmail.com',
					  'how_are_we_doing_3_59' => 'BOBO RONDREA LERAI',
					  'how_are_we_doing_4_59' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_59' => 'Justin Knight',
					  'how_are_we_doing_6_59' => '4.0',
					  'how_are_we_doing_0_60' => '2',
					  'how_are_we_doing_1_60' => '29/05/2019',
					  'how_are_we_doing_2_60' => 'rachelovesjc@gmail.com',
					  'how_are_we_doing_3_60' => 'PARK RACHEL E',
					  'how_are_we_doing_4_60' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_60' => 'Justin Knight',
					  'how_are_we_doing_6_60' => '4.0',
					  'how_are_we_doing_0_61' => '2',
					  'how_are_we_doing_1_61' => '29/05/2019',
					  'how_are_we_doing_2_61' => 'rparedesra@gmail.com',
					  'how_are_we_doing_3_61' => 'CARDHOLDER VISA',
					  'how_are_we_doing_4_61' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_61' => 'Justin Knight',
					  'how_are_we_doing_6_61' => '4.0',
					  'how_are_we_doing_0_62' => '2',
					  'how_are_we_doing_1_62' => '29/05/2019',
					  'how_are_we_doing_2_62' => 'sharnellc15@gmail.com',
					  'how_are_we_doing_3_62' => 'CAMPBELL SHARNELL A',
					  'how_are_we_doing_4_62' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_62' => 'Justin Knight',
					  'how_are_we_doing_6_62' => '4.0',
					  'how_are_we_doing_0_63' => '2',
					  'how_are_we_doing_1_63' => '29/05/2019',
					  'how_are_we_doing_2_63' => 'suchprettythings@yahoo.com',
					  'how_are_we_doing_3_63' => 'ENIG JESSICA L',
					  'how_are_we_doing_4_63' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_63' => 'Justin Knight',
					  'how_are_we_doing_6_63' => '4.0',
					  'how_are_we_doing_0_64' => '2',
					  'how_are_we_doing_1_64' => '29/05/2019',
					  'how_are_we_doing_2_64' => 'tanyayaguilar@yahoo.com',
					  'how_are_we_doing_3_64' => 'AGUILAR TANYA',
					  'how_are_we_doing_4_64' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_64' => 'Justin Knight',
					  'how_are_we_doing_6_64' => '4.0',
					  'how_are_we_doing_0_65' => '2',
					  'how_are_we_doing_1_65' => '29/05/2019',
					  'how_are_we_doing_2_65' => 'tarmstrong@konterrarealty.com',
					  'how_are_we_doing_3_65' => 'ARMSTRONG TATE',
					  'how_are_we_doing_4_65' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_65' => 'Justin Knight',
					  'how_are_we_doing_6_65' => '4.0',
					  'how_are_we_doing_0_66' => '2',
					  'how_are_we_doing_1_66' => '29/05/2019',
					  'how_are_we_doing_2_66' => 'theinrich59@aol.com',
					  'how_are_we_doing_3_66' => 'HEINRICH PIETRINA',
					  'how_are_we_doing_4_66' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_66' => 'Justin Knight',
					  'how_are_we_doing_6_66' => '4.0',
					  'how_are_we_doing_0_67' => '2',
					  'how_are_we_doing_1_67' => '29/05/2019',
					  'how_are_we_doing_2_67' => 'tishways@hotmail.com',
					  'how_are_we_doing_3_67' => 'WAYS LETITIA S',
					  'how_are_we_doing_4_67' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_67' => 'Justin Knight',
					  'how_are_we_doing_6_67' => '2.0',
					  'how_are_we_doing_0_68' => '2',
					  'how_are_we_doing_1_68' => '29/05/2019',
					  'how_are_we_doing_2_68' => 'tmancuso37@hotmail.com',
					  'how_are_we_doing_3_68' => 'MANCUSO ANTHONY S',
					  'how_are_we_doing_4_68' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_68' => 'Justin Knight',
					  'how_are_we_doing_6_68' => '4.0',
					  'how_are_we_doing_0_69' => '2',
					  'how_are_we_doing_1_69' => '29/05/2019',
					  'how_are_we_doing_2_69' => 'vanna.chung@gmail.com',
					  'how_are_we_doing_3_69' => 'CHUNG VANNA',
					  'how_are_we_doing_4_69' => 'Uncle Julio\'s Bethesda',
					  'how_are_we_doing_5_69' => 'Justin Knight',
					  'how_are_we_doing_6_69' => '3.5',
					  'how_are_we_doing_0_70' => '4',
					  'how_are_we_doing_1_70' => '29/05/2019',
					  'how_are_we_doing_2_70' => '052909natalee@gmail.com',
					  'how_are_we_doing_3_70' => 'YUTZY JASON',
					  'how_are_we_doing_4_70' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_70' => 'Orio Lodal',
					  'how_are_we_doing_6_70' => '4.0',
					  'how_are_we_doing_0_71' => '4',
					  'how_are_we_doing_1_71' => '29/05/2019',
					  'how_are_we_doing_2_71' => 'ashtonlovesmath@gmail.com',
					  'how_are_we_doing_3_71' => 'KING ASHTON',
					  'how_are_we_doing_4_71' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_71' => 'Orio Lodal',
					  'how_are_we_doing_6_71' => '4.0',
					  'how_are_we_doing_0_72' => '4',
					  'how_are_we_doing_1_72' => '29/05/2019',
					  'how_are_we_doing_2_72' => 'axkelly01@yahoo.com',
					  'how_are_we_doing_3_72' => 'KELLY ANGELA',
					  'how_are_we_doing_4_72' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_72' => 'Orio Lodal',
					  'how_are_we_doing_6_72' => '3.0',
					  'how_are_we_doing_0_73' => '4',
					  'how_are_we_doing_1_73' => '29/05/2019',
					  'how_are_we_doing_2_73' => 'brandikyles@gmail.com',
					  'how_are_we_doing_3_73' => 'KYLES BRANDI R',
					  'how_are_we_doing_4_73' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_73' => 'Orio Lodal',
					  'how_are_we_doing_6_73' => '4.0',
					  'how_are_we_doing_0_74' => '4',
					  'how_are_we_doing_1_74' => '29/05/2019',
					  'how_are_we_doing_2_74' => 'cbc_clevelan@yahoo.com',
					  'how_are_we_doing_3_74' => 'CLEVELAND CASSIE B',
					  'how_are_we_doing_4_74' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_74' => 'Orio Lodal',
					  'how_are_we_doing_6_74' => '4.0',
					  'how_are_we_doing_0_75' => '4',
					  'how_are_we_doing_1_75' => '29/05/2019',
					  'how_are_we_doing_2_75' => 'daphcret@gmail.com',
					  'how_are_we_doing_3_75' => 'CRETSINGER DAPHNE L',
					  'how_are_we_doing_4_75' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_75' => 'Orio Lodal',
					  'how_are_we_doing_6_75' => '4.0',
					  'how_are_we_doing_0_76' => '4',
					  'how_are_we_doing_1_76' => '29/05/2019',
					  'how_are_we_doing_2_76' => 'donatchison@hotmail.com',
					  'how_are_we_doing_3_76' => 'ATCHISON DON A',
					  'how_are_we_doing_4_76' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_76' => 'Orio Lodal',
					  'how_are_we_doing_6_76' => '4.0',
					  'how_are_we_doing_0_77' => '4',
					  'how_are_we_doing_1_77' => '29/05/2019',
					  'how_are_we_doing_2_77' => 'edmundof85@hotmail.com',
					  'how_are_we_doing_3_77' => 'FAVELA EDMUNDO',
					  'how_are_we_doing_4_77' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_77' => 'Orio Lodal',
					  'how_are_we_doing_6_77' => '4.0',
					  'how_are_we_doing_0_78' => '4',
					  'how_are_we_doing_1_78' => '29/05/2019',
					  'how_are_we_doing_2_78' => 'elsiedenise@hotmail.com',
					  'how_are_we_doing_3_78' => 'DUNACUSKY ELSIE',
					  'how_are_we_doing_4_78' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_78' => 'Orio Lodal',
					  'how_are_we_doing_6_78' => '4.0',
					  'how_are_we_doing_0_79' => '4',
					  'how_are_we_doing_1_79' => '29/05/2019',
					  'how_are_we_doing_2_79' => 'fleming_deidra@yahoo.com',
					  'how_are_we_doing_3_79' => 'FLEMING DEIDRA LYNN',
					  'how_are_we_doing_4_79' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_79' => 'Orio Lodal',
					  'how_are_we_doing_6_79' => '3.0',
					  'how_are_we_doing_0_80' => '4',
					  'how_are_we_doing_1_80' => '29/05/2019',
					  'how_are_we_doing_2_80' => 'glo1martin@yahoo.com',
					  'how_are_we_doing_3_80' => 'MARTINEZ GLORIA',
					  'how_are_we_doing_4_80' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_80' => 'Orio Lodal',
					  'how_are_we_doing_6_80' => '4.0',
					  'how_are_we_doing_0_81' => '4',
					  'how_are_we_doing_1_81' => '29/05/2019',
					  'how_are_we_doing_2_81' => 'golfer81588@yahoo.com',
					  'how_are_we_doing_3_81' => 'MEYER GARRETT J',
					  'how_are_we_doing_4_81' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_81' => 'Orio Lodal',
					  'how_are_we_doing_6_81' => '4.0',
					  'how_are_we_doing_0_82' => '4',
					  'how_are_we_doing_1_82' => '29/05/2019',
					  'how_are_we_doing_2_82' => 'gthornton@westovergrp.com',
					  'how_are_we_doing_3_82' => 'THORNTON GLENNA',
					  'how_are_we_doing_4_82' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_82' => 'Orio Lodal',
					  'how_are_we_doing_6_82' => '4.0',
					  'how_are_we_doing_0_83' => '4',
					  'how_are_we_doing_1_83' => '29/05/2019',
					  'how_are_we_doing_2_83' => 'heweekly@att.net',
					  'how_are_we_doing_3_83' => 'WEEKLY HARRY EARL',
					  'how_are_we_doing_4_83' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_83' => 'Orio Lodal',
					  'how_are_we_doing_6_83' => '4.0',
					  'how_are_we_doing_0_84' => '4',
					  'how_are_we_doing_1_84' => '29/05/2019',
					  'how_are_we_doing_2_84' => 'heweekly@att.netheee',
					  'how_are_we_doing_3_84' => 'WEEKLY HARRY EARL',
					  'how_are_we_doing_4_84' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_84' => 'Orio Lodal',
					  'how_are_we_doing_6_84' => '4.0',
					  'how_are_we_doing_0_85' => '4',
					  'how_are_we_doing_1_85' => '29/05/2019',
					  'how_are_we_doing_2_85' => 'jalmeida2621@gmail.com',
					  'how_are_we_doing_3_85' => 'ALMEIDA JAIME',
					  'how_are_we_doing_4_85' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_85' => 'Orio Lodal',
					  'how_are_we_doing_6_85' => '4.0',
					  'how_are_we_doing_0_86' => '4',
					  'how_are_we_doing_1_86' => '29/05/2019',
					  'how_are_we_doing_2_86' => 'javarn@sbcglobal.net',
					  'how_are_we_doing_3_86' => 'VAZQUEZ JAIME',
					  'how_are_we_doing_4_86' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_86' => 'Orio Lodal',
					  'how_are_we_doing_6_86' => '3.0',
					  'how_are_we_doing_0_87' => '4',
					  'how_are_we_doing_1_87' => '29/05/2019',
					  'how_are_we_doing_2_87' => 'jgsbgs@yahoo.com',
					  'how_are_we_doing_3_87' => 'SKINNER JOHNNIE G',
					  'how_are_we_doing_4_87' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_87' => 'Orio Lodal',
					  'how_are_we_doing_6_87' => '3.0',
					  'how_are_we_doing_0_88' => '4',
					  'how_are_we_doing_1_88' => '29/05/2019',
					  'how_are_we_doing_2_88' => 'john.sutton@purina.nestle.com',
					  'how_are_we_doing_3_88' => 'SUTTON JOHN',
					  'how_are_we_doing_4_88' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_88' => 'Orio Lodal',
					  'how_are_we_doing_6_88' => '4.0',
					  'how_are_we_doing_0_89' => '4',
					  'how_are_we_doing_1_89' => '29/05/2019',
					  'how_are_we_doing_2_89' => 'kcaraveo777@gmail.com',
					  'how_are_we_doing_3_89' => 'FLORES KAREN A',
					  'how_are_we_doing_4_89' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_89' => 'Orio Lodal',
					  'how_are_we_doing_6_89' => '4.0',
					  'how_are_we_doing_0_90' => '4',
					  'how_are_we_doing_1_90' => '29/05/2019',
					  'how_are_we_doing_2_90' => 'keishon78@gmail.com',
					  'how_are_we_doing_3_90' => 'SPRIGGS MARKEISHA',
					  'how_are_we_doing_4_90' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_90' => 'Orio Lodal',
					  'how_are_we_doing_6_90' => '2.0',
					  'how_are_we_doing_0_91' => '4',
					  'how_are_we_doing_1_91' => '29/05/2019',
					  'how_are_we_doing_2_91' => 'knichols326@charter.net',
					  'how_are_we_doing_3_91' => 'NICHOLS DIANNA K',
					  'how_are_we_doing_4_91' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_91' => 'Orio Lodal',
					  'how_are_we_doing_6_91' => '4.0',
					  'how_are_we_doing_0_92' => '4',
					  'how_are_we_doing_1_92' => '29/05/2019',
					  'how_are_we_doing_2_92' => 'lester.kuperman@gmail.com',
					  'how_are_we_doing_3_92' => 'KUPERMAN LESTER H',
					  'how_are_we_doing_4_92' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_92' => 'Orio Lodal',
					  'how_are_we_doing_6_92' => '3.0',
					  'how_are_we_doing_0_93' => '4',
					  'how_are_we_doing_1_93' => '29/05/2019',
					  'how_are_we_doing_2_93' => 'ljboschertjr@aol.com',
					  'how_are_we_doing_3_93' => 'L  D RED WING SHOE',
					  'how_are_we_doing_4_93' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_93' => 'Orio Lodal',
					  'how_are_we_doing_6_93' => '4.0',
					  'how_are_we_doing_0_94' => '4',
					  'how_are_we_doing_1_94' => '29/05/2019',
					  'how_are_we_doing_2_94' => 'mriguess@yahoo.com',
					  'how_are_we_doing_3_94' => 'MEDINA  CHRISTIFER',
					  'how_are_we_doing_4_94' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_94' => 'Orio Lodal',
					  'how_are_we_doing_6_94' => '4.0',
					  'how_are_we_doing_0_95' => '4',
					  'how_are_we_doing_1_95' => '29/05/2019',
					  'how_are_we_doing_2_95' => 'nancy.gracia@gmail.com',
					  'how_are_we_doing_3_95' => 'GRACIA  NANCY',
					  'how_are_we_doing_4_95' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_95' => 'Orio Lodal',
					  'how_are_we_doing_6_95' => '4.0',
					  'how_are_we_doing_0_96' => '4',
					  'how_are_we_doing_1_96' => '29/05/2019',
					  'how_are_we_doing_2_96' => 'ricky3181996@gmail.com',
					  'how_are_we_doing_3_96' => 'CAVITA  RICARDO',
					  'how_are_we_doing_4_96' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_96' => 'Orio Lodal',
					  'how_are_we_doing_6_96' => '4.0',
					  'how_are_we_doing_0_97' => '4',
					  'how_are_we_doing_1_97' => '29/05/2019',
					  'how_are_we_doing_2_97' => 'rsrearden@att.net',
					  'how_are_we_doing_3_97' => 'REARDEN JR ROBERT S',
					  'how_are_we_doing_4_97' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_97' => 'Orio Lodal',
					  'how_are_we_doing_6_97' => '4.0',
					  'how_are_we_doing_0_98' => '4',
					  'how_are_we_doing_1_98' => '29/05/2019',
					  'how_are_we_doing_2_98' => 'store@barbarascustomhats.com',
					  'how_are_we_doing_3_98' => 'ALMOS CHRISTINA',
					  'how_are_we_doing_4_98' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_98' => 'Orio Lodal',
					  'how_are_we_doing_6_98' => '4.0',
					  'how_are_we_doing_0_99' => '4',
					  'how_are_we_doing_1_99' => '29/05/2019',
					  'how_are_we_doing_2_99' => 'swilsondanielle@gmail.com',
					  'how_are_we_doing_3_99' => 'KINZIE WALTER',
					  'how_are_we_doing_4_99' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_99' => 'Orio Lodal',
					  'how_are_we_doing_6_99' => '3.0',
					  'how_are_we_doing_0_100' => '4',
					  'how_are_we_doing_1_100' => '29/05/2019',
					  'how_are_we_doing_2_100' => 'viankanery@hotmail.com',
					  'how_are_we_doing_3_100' => 'MORENO RAMON',
					  'how_are_we_doing_4_100' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_100' => 'Orio Lodal',
					  'how_are_we_doing_6_100' => '4.0',
					  'how_are_we_doing_0_101' => '4',
					  'how_are_we_doing_1_101' => '29/05/2019',
					  'how_are_we_doing_2_101' => 'vrgrn1@aol.com',
					  'how_are_we_doing_3_101' => 'GREEN V',
					  'how_are_we_doing_4_101' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_101' => 'Orio Lodal',
					  'how_are_we_doing_6_101' => '4.0',
					  'how_are_we_doing_0_102' => '4',
					  'how_are_we_doing_1_102' => '29/05/2019',
					  'how_are_we_doing_2_102' => 'willbike4food@cableone.net',
					  'how_are_we_doing_3_102' => 'HANKE THOMAS',
					  'how_are_we_doing_4_102' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_102' => 'Orio Lodal',
					  'how_are_we_doing_6_102' => '4.0',
					  'how_are_we_doing_0_103' => '4',
					  'how_are_we_doing_1_103' => '29/05/2019',
					  'how_are_we_doing_2_103' => 'williamgibbs1979@gmail.com',
					  'how_are_we_doing_3_103' => 'GIBBS WILLIAM',
					  'how_are_we_doing_4_103' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_103' => 'Orio Lodal',
					  'how_are_we_doing_6_103' => '3.0',
					  'how_are_we_doing_0_104' => '4',
					  'how_are_we_doing_1_104' => '29/05/2019',
					  'how_are_we_doing_2_104' => 'zp@mandplawyers.com',
					  'how_are_we_doing_3_104' => 'PETTIGREW  ZACHARY',
					  'how_are_we_doing_4_104' => 'Uncle Julio\'s Fort Worth',
					  'how_are_we_doing_5_104' => 'Orio Lodal',
					  'how_are_we_doing_6_104' => '3.0',
					  'how_are_we_doing_0_105' => '5',
					  'how_are_we_doing_1_105' => '29/05/2019',
					  'how_are_we_doing_2_105' => 'adamculby@gmail.com',
					  'how_are_we_doing_3_105' => 'CULBERTSON ADAM',
					  'how_are_we_doing_4_105' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_105' => 'Jose Fernandez',
					  'how_are_we_doing_6_105' => '4.0',
					  'how_are_we_doing_0_106' => '5',
					  'how_are_we_doing_1_106' => '29/05/2019',
					  'how_are_we_doing_2_106' => 'amanda.radmand@gmail.com',
					  'how_are_we_doing_3_106' => 'RADMAND AMANDA C',
					  'how_are_we_doing_4_106' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_106' => 'Jose Fernandez',
					  'how_are_we_doing_6_106' => '4.0',
					  'how_are_we_doing_0_107' => '5',
					  'how_are_we_doing_1_107' => '29/05/2019',
					  'how_are_we_doing_2_107' => 'anaiahgrissom@gmail.com',
					  'how_are_we_doing_3_107' => 'GRISSOM ANAIAH L',
					  'how_are_we_doing_4_107' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_107' => 'Jose Fernandez',
					  'how_are_we_doing_6_107' => '4.0',
					  'how_are_we_doing_0_108' => '5',
					  'how_are_we_doing_1_108' => '29/05/2019',
					  'how_are_we_doing_2_108' => 'andrew.slaughter@gmail.com',
					  'how_are_we_doing_3_108' => 'SLAUGHTER ANDREW J',
					  'how_are_we_doing_4_108' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_108' => 'Jose Fernandez',
					  'how_are_we_doing_6_108' => '4.0',
					  'how_are_we_doing_0_109' => '5',
					  'how_are_we_doing_1_109' => '29/05/2019',
					  'how_are_we_doing_2_109' => 'btrimble86@gmail.com',
					  'how_are_we_doing_3_109' => 'TRIMBLE BRANDON',
					  'how_are_we_doing_4_109' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_109' => 'Jose Fernandez',
					  'how_are_we_doing_6_109' => '4.0',
					  'how_are_we_doing_0_110' => '5',
					  'how_are_we_doing_1_110' => '29/05/2019',
					  'how_are_we_doing_2_110' => 'carnell.thornton@gmail.com',
					  'how_are_we_doing_3_110' => 'THORNTON  CARNELL',
					  'how_are_we_doing_4_110' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_110' => 'Jose Fernandez',
					  'how_are_we_doing_6_110' => '4.0',
					  'how_are_we_doing_0_111' => '5',
					  'how_are_we_doing_1_111' => '29/05/2019',
					  'how_are_we_doing_2_111' => 'cohara44@aol.com',
					  'how_are_we_doing_3_111' => 'O\'HARA CAROLINE L',
					  'how_are_we_doing_4_111' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_111' => 'Jose Fernandez',
					  'how_are_we_doing_6_111' => '4.0',
					  'how_are_we_doing_0_112' => '5',
					  'how_are_we_doing_1_112' => '29/05/2019',
					  'how_are_we_doing_2_112' => 'cwbalisle@aol.com',
					  'how_are_we_doing_3_112' => 'BALISLE CHRISTINE W',
					  'how_are_we_doing_4_112' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_112' => 'Jose Fernandez',
					  'how_are_we_doing_6_112' => '2.0',
					  'how_are_we_doing_0_113' => '5',
					  'how_are_we_doing_1_113' => '29/05/2019',
					  'how_are_we_doing_2_113' => 'elizabeth.gibbs24@gmail.com',
					  'how_are_we_doing_3_113' => 'HAIR BY LIZZIE',
					  'how_are_we_doing_4_113' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_113' => 'Jose Fernandez',
					  'how_are_we_doing_6_113' => '3.0',
					  'how_are_we_doing_0_114' => '5',
					  'how_are_we_doing_1_114' => '29/05/2019',
					  'how_are_we_doing_2_114' => 'erikaucramer@gmail.com',
					  'how_are_we_doing_3_114' => 'CRAMER ERIKA',
					  'how_are_we_doing_4_114' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_114' => 'Jose Fernandez',
					  'how_are_we_doing_6_114' => '3.0',
					  'how_are_we_doing_0_115' => '5',
					  'how_are_we_doing_1_115' => '29/05/2019',
					  'how_are_we_doing_2_115' => 'eroeder@jensenhughes.com',
					  'how_are_we_doing_3_115' => 'ROEDER ERIC',
					  'how_are_we_doing_4_115' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_115' => 'Jose Fernandez',
					  'how_are_we_doing_6_115' => '4.0',
					  'how_are_we_doing_0_116' => '5',
					  'how_are_we_doing_1_116' => '29/05/2019',
					  'how_are_we_doing_2_116' => 'gakinyemi@inbox.com',
					  'how_are_we_doing_3_116' => 'AKINYEMI GBOLAHAN',
					  'how_are_we_doing_4_116' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_116' => 'Jose Fernandez',
					  'how_are_we_doing_6_116' => '4.0',
					  'how_are_we_doing_0_117' => '5',
					  'how_are_we_doing_1_117' => '29/05/2019',
					  'how_are_we_doing_2_117' => 'gvaldivia5@yahoo.com',
					  'how_are_we_doing_3_117' => 'VALDIVIA GUILLERMO A',
					  'how_are_we_doing_4_117' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_117' => 'Jose Fernandez',
					  'how_are_we_doing_6_117' => '4.0',
					  'how_are_we_doing_0_118' => '5',
					  'how_are_we_doing_1_118' => '29/05/2019',
					  'how_are_we_doing_2_118' => 'itsmeindc@gmail.com',
					  'how_are_we_doing_3_118' => 'BARRY CYNTHIA',
					  'how_are_we_doing_4_118' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_118' => 'Jose Fernandez',
					  'how_are_we_doing_6_118' => '3.0',
					  'how_are_we_doing_0_119' => '5',
					  'how_are_we_doing_1_119' => '29/05/2019',
					  'how_are_we_doing_2_119' => 'jholstonjr@gmail.com',
					  'how_are_we_doing_3_119' => 'HOLSTON CHRISTIE',
					  'how_are_we_doing_4_119' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_119' => 'Jose Fernandez',
					  'how_are_we_doing_6_119' => '4.0',
					  'how_are_we_doing_0_120' => '5',
					  'how_are_we_doing_1_120' => '29/05/2019',
					  'how_are_we_doing_2_120' => 'jowest-lesher@campus.peru.edu',
					  'how_are_we_doing_3_120' => 'WEST LESHER JOSHUA C',
					  'how_are_we_doing_4_120' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_120' => 'Jose Fernandez',
					  'how_are_we_doing_6_120' => '4.0',
					  'how_are_we_doing_0_121' => '5',
					  'how_are_we_doing_1_121' => '29/05/2019',
					  'how_are_we_doing_2_121' => 'jpiovino@yahoo.com',
					  'how_are_we_doing_3_121' => 'PHILIP IOVINO JAMES',
					  'how_are_we_doing_4_121' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_121' => 'Jose Fernandez',
					  'how_are_we_doing_6_121' => '4.0',
					  'how_are_we_doing_0_122' => '5',
					  'how_are_we_doing_1_122' => '29/05/2019',
					  'how_are_we_doing_2_122' => 'kevin.vunck@effem.com',
					  'how_are_we_doing_3_122' => 'VUNCK KEVIN',
					  'how_are_we_doing_4_122' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_122' => 'Jose Fernandez',
					  'how_are_we_doing_6_122' => '4.0',
					  'how_are_we_doing_0_123' => '5',
					  'how_are_we_doing_1_123' => '29/05/2019',
					  'how_are_we_doing_2_123' => 'kmcdermith@outlook.com',
					  'how_are_we_doing_3_123' => 'MCDERMITH III KENNETH',
					  'how_are_we_doing_4_123' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_123' => 'Jose Fernandez',
					  'how_are_we_doing_6_123' => '4.0',
					  'how_are_we_doing_0_124' => '5',
					  'how_are_we_doing_1_124' => '29/05/2019',
					  'how_are_we_doing_2_124' => 'kodiequeen@gmail.com',
					  'how_are_we_doing_3_124' => '',
					  'how_are_we_doing_4_124' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_124' => 'Jose Fernandez',
					  'how_are_we_doing_6_124' => '4.0',
					  'how_are_we_doing_0_125' => '5',
					  'how_are_we_doing_1_125' => '29/05/2019',
					  'how_are_we_doing_2_125' => 'laraoffutt@me.com',
					  'how_are_we_doing_3_125' => 'OFFUTT LARA',
					  'how_are_we_doing_4_125' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_125' => 'Jose Fernandez',
					  'how_are_we_doing_6_125' => '4.0',
					  'how_are_we_doing_0_126' => '5',
					  'how_are_we_doing_1_126' => '29/05/2019',
					  'how_are_we_doing_2_126' => 'larryb@live.com',
					  'how_are_we_doing_3_126' => 'BOWLES LARRY',
					  'how_are_we_doing_4_126' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_126' => 'Jose Fernandez',
					  'how_are_we_doing_6_126' => '4.0',
					  'how_are_we_doing_0_127' => '5',
					  'how_are_we_doing_1_127' => '29/05/2019',
					  'how_are_we_doing_2_127' => 'lincas@yahoo.com',
					  'how_are_we_doing_3_127' => 'FROIO JASON J',
					  'how_are_we_doing_4_127' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_127' => 'Jose Fernandez',
					  'how_are_we_doing_6_127' => '4.0',
					  'how_are_we_doing_0_128' => '5',
					  'how_are_we_doing_1_128' => '29/05/2019',
					  'how_are_we_doing_2_128' => 'marktringale@aol.com',
					  'how_are_we_doing_3_128' => 'TRINGALE MARK',
					  'how_are_we_doing_4_128' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_128' => 'Jose Fernandez',
					  'how_are_we_doing_6_128' => '4.0',
					  'how_are_we_doing_0_129' => '5',
					  'how_are_we_doing_1_129' => '29/05/2019',
					  'how_are_we_doing_2_129' => 'mcirtain@gmail.com',
					  'how_are_we_doing_3_129' => 'CIRTAIN MELISSA',
					  'how_are_we_doing_4_129' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_129' => 'Jose Fernandez',
					  'how_are_we_doing_6_129' => '4.0',
					  'how_are_we_doing_0_130' => '5',
					  'how_are_we_doing_1_130' => '29/05/2019',
					  'how_are_we_doing_2_130' => 'michellebosworth@gmail.com',
					  'how_are_we_doing_3_130' => 'BOSWORTH BONNIE',
					  'how_are_we_doing_4_130' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_130' => 'Jose Fernandez',
					  'how_are_we_doing_6_130' => '3.0',
					  'how_are_we_doing_0_131' => '5',
					  'how_are_we_doing_1_131' => '29/05/2019',
					  'how_are_we_doing_2_131' => 'officemanager@dominionanesthesia.com',
					  'how_are_we_doing_3_131' => 'LEE DAVID',
					  'how_are_we_doing_4_131' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_131' => 'Jose Fernandez',
					  'how_are_we_doing_6_131' => '3.0',
					  'how_are_we_doing_0_132' => '5',
					  'how_are_we_doing_1_132' => '29/05/2019',
					  'how_are_we_doing_2_132' => 'paul.krein@redriver.com',
					  'how_are_we_doing_3_132' => 'KREIN PAUL',
					  'how_are_we_doing_4_132' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_132' => 'Jose Fernandez',
					  'how_are_we_doing_6_132' => '3.0',
					  'how_are_we_doing_0_133' => '5',
					  'how_are_we_doing_1_133' => '29/05/2019',
					  'how_are_we_doing_2_133' => 'peter.pettas@ey.com',
					  'how_are_we_doing_3_133' => 'PETTAS PETER A',
					  'how_are_we_doing_4_133' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_133' => 'Jose Fernandez',
					  'how_are_we_doing_6_133' => '4.0',
					  'how_are_we_doing_0_134' => '5',
					  'how_are_we_doing_1_134' => '29/05/2019',
					  'how_are_we_doing_2_134' => 'ray.mccollum@gmail.com',
					  'how_are_we_doing_3_134' => 'MCCOLLUM RAYMOND',
					  'how_are_we_doing_4_134' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_134' => 'Jose Fernandez',
					  'how_are_we_doing_6_134' => '4.0',
					  'how_are_we_doing_0_135' => '5',
					  'how_are_we_doing_1_135' => '29/05/2019',
					  'how_are_we_doing_2_135' => 'roger_bridges@yahoo.com',
					  'how_are_we_doing_3_135' => 'BRIDGES ROGER',
					  'how_are_we_doing_4_135' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_135' => 'Jose Fernandez',
					  'how_are_we_doing_6_135' => '4.0',
					  'how_are_we_doing_0_136' => '5',
					  'how_are_we_doing_1_136' => '29/05/2019',
					  'how_are_we_doing_2_136' => 's.arrington84@gmail.com',
					  'how_are_we_doing_3_136' => 'ARRINGTON SHANNON D',
					  'how_are_we_doing_4_136' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_136' => 'Jose Fernandez',
					  'how_are_we_doing_6_136' => '4.0',
					  'how_are_we_doing_0_137' => '5',
					  'how_are_we_doing_1_137' => '29/05/2019',
					  'how_are_we_doing_2_137' => 'samantha.nangia@gmail.com',
					  'how_are_we_doing_3_137' => '',
					  'how_are_we_doing_4_137' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_137' => 'Jose Fernandez',
					  'how_are_we_doing_6_137' => '4.0',
					  'how_are_we_doing_0_138' => '5',
					  'how_are_we_doing_1_138' => '29/05/2019',
					  'how_are_we_doing_2_138' => 'stephanie.croghan@am.jll.com',
					  'how_are_we_doing_3_138' => 'CROGHAN STEPHANIE L',
					  'how_are_we_doing_4_138' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_138' => 'Jose Fernandez',
					  'how_are_we_doing_6_138' => '4.0',
					  'how_are_we_doing_0_139' => '5',
					  'how_are_we_doing_1_139' => '29/05/2019',
					  'how_are_we_doing_2_139' => 'tony.hudnall.3@us.af.mil',
					  'how_are_we_doing_3_139' => 'HUDNALL TONY J',
					  'how_are_we_doing_4_139' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_139' => 'Jose Fernandez',
					  'how_are_we_doing_6_139' => '3.0',
					  'how_are_we_doing_0_140' => '5',
					  'how_are_we_doing_1_140' => '29/05/2019',
					  'how_are_we_doing_2_140' => 'vmvillat@gmail.com',
					  'how_are_we_doing_3_140' => 'FLORES MILAGRO',
					  'how_are_we_doing_4_140' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_140' => 'Jose Fernandez',
					  'how_are_we_doing_6_140' => '4.0',
					  'how_are_we_doing_0_141' => '5',
					  'how_are_we_doing_1_141' => '29/05/2019',
					  'how_are_we_doing_2_141' => 'wmccoy@belcan.com',
					  'how_are_we_doing_3_141' => 'MCCOY  WANDA',
					  'how_are_we_doing_4_141' => 'Uncle Julio\'s Arlington',
					  'how_are_we_doing_5_141' => 'Jose Fernandez',
					  'how_are_we_doing_6_141' => '4.0',
					  'how_are_we_doing_0_142' => '6',
					  'how_are_we_doing_1_142' => '29/05/2019',
					  'how_are_we_doing_2_142' => 'alison.wright@tcg.com',
					  'how_are_we_doing_3_142' => 'WRIGHT ALISON',
					  'how_are_we_doing_4_142' => 'Uncle Julio\'s Reston',
					  'how_are_we_doing_5_142' => 'Larry Graham',
					  'how_are_we_doing_6_142' => '4.0',
					);

		
	}

}	



