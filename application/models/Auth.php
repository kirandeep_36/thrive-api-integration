<?php 

	class Auth extends CI_Model {

		public function __construct() {
			parent::__construct();
		}

		// Get Data
		public function get_data($table){
			$query = $this->db->get($table);
			return $query->result_array();
		}

		// Function get user by ID
		public function get_current_user($table, $id){
			$this->db->where('id', $id);
			$this->db->from($table);
			$query = $this->db->get();
			return $query->row_array();
		}

		// Function get Profile ID by Store ID
		public function get_profile_id($table, $id){
			$this->db->select('profile_id');
			$this->db->where('store_id', $id);
			$this->db->from($table);
			$query = $this->db->get();
			return $query->row_array();
		}


		// Insert 
		public function insert_function($table, $data){
			$fields = array_keys($data);
			$values = array_values($data);
			$this->db->insert($table, $data);
			$qry = $this->db->affected_rows();
			return $qry;
		}


		// Delete
		public function delt_function($table, $id){
			$this->db->where('id', $id);
			$this->db->delete($table);	
			$qry = $this->db->affected_rows();
			return $qry;
		}


		//Update
		public function update_function($table, $data, $id){
			$qry = $this->db->update($table, $data, array('id' => $id));
			$qry = $this->db->affected_rows();
			return $qry;
		}


		//Login check
		public function check_login($data, $table){
			$this->db->where($data);
			$this->db->from($table);
			$result = $this->db->get();
			$result = $result->row_array();
			return $result;
		}

	}

?>